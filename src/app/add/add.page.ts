import { Component, OnInit } from '@angular/core';
import {TodoService} from '../todo.service'
import { Todo } from '../todo';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  public todo : Todo;

  constructor(private todoService : TodoService, private toastController: ToastController, private router : Router) { }

  ngOnInit() {
    this.todo = new Todo();
  }

  addTask() {
    this.todoService.addTodo(this.todo).then(() => {
      this.router.navigateByUrl('/');
      this.showToast('New task added');
    });
  }

  showToast(msg) {
    this.toastController.create({
      message: msg,
      duration: 2000,
      position: "top"
    }).then(toast => toast.present());
  }
}


