import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Todo } from './todo';

@Injectable({
  providedIn: 'root'
})

export class TodoService {

  private todosCollection : AngularFirestoreCollection<Todo>;
  private todos : Observable<Todo[]>;

  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<Todo>('todos');

    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getTodos(): Observable<Todo[]> {
    return this.todos;
  }
 
  getTodo(id: string): Observable<Todo> {
    return this.todosCollection.doc<Todo>(id).valueChanges().pipe(
      take(1),
      map(todo => {
        todo.id = id;
        return todo;
      })
    );
  }
 
  addTodo(todo: Todo): Promise<DocumentReference> {
    return this.todosCollection.add({...todo});
  }
 
  /* not used, here as an example */
  updateTodo(todo: Todo): Promise<void> {
    return this.todosCollection.doc(todo.id).update({ name: todo.task });
  }
 
  deleteTodo(id: string): Promise<void> {
    console.log("delete todo " + id);
    return this.todosCollection.doc(id).delete();
  }
}
