// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase:{
    apiKey: "AIzaSyD-VPdBcCjQlZC6piTnsd_E9Bl-RVEcw0Q",
    authDomain: "todo-list-ae3cf.firebaseapp.com",
    databaseURL: "https://todo-list-ae3cf.firebaseio.com",
    projectId: "todo-list-ae3cf",
    storageBucket: "todo-list-ae3cf.appspot.com",
    messagingSenderId: "779291882992",
    appId: "1:779291882992:web:a894b308191420fd0d5935"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

/* firebase:{
  apiKey: "AIzaSyA4UJBb829lyu5hjOaOVTT12TmOtwQdurg",
  authDomain: "ideapool-baf64.firebaseapp.com",
  databaseURL: "https://ideapool-baf64.firebaseio.com",
  projectId: "ideapool-baf64",
  storageBucket: "ideapool-baf64.appspot.com",
  messagingSenderId: "332969045723",
  appId: "1:332969045723:web:2a1df80afe1a255cb3e6fe"
} */
